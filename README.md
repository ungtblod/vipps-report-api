<!-- START_METADATA
---
title: Introduction
sidebar_position: 1
---
END_METADATA -->

# Vipps Report API

<!-- START_COMMENT -->

ℹ️ Please use the new documentation:
[Vipps Technical Documentation](https://vippsas.github.io/vipps-developer-docs/).

<!-- END_COMMENT -->

💥 DRAFT! Unfinished work in progress. API specification changes are still coming. 💥

The report API gives you or a third-party acting on your behalf the ability to fetch information about payment events that have been processed by Vipps.

## Getting started

Review the [Getting Started](https://github.com/vippsas/vipps-developers/blob/master/vipps-getting-started.md) pages for information about API keys, product activation, contact information, contribution guidelines, etc.

## Report API documentation

* [API Guide: Overview](vipps-report-api.md)
* [API Guide: Settlements](vipps-report-api-settlement-guide.md)
* [API Spec](https://vippsas.github.io/vipps-report-api/)
* [API Quick start](vipps-report-api-quick-start.md)
* [API FAQ](vipps-report-api-faq.md): Frequently asked questions

## Questions?

We're always happy to help with code or other questions you might have!
Please create an [issue](https://github.com/vippsas/vipps-report-api/issues),
a [pull request](https://github.com/vippsas/vipps-report-api/pulls),
or [contact us](https://github.com/vippsas/vipps-report/blob/master/contact.md).
